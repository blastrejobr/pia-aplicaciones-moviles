import { PlanetsService } from './../planets.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { IPlanet } from '../planets.model';

@Component({
  selector: 'app-planet-details',
  templateUrl: './planet-details.page.html',
  styleUrls: ['./planet-details.page.scss'],
})
export class PlanetDetailsPage implements OnInit {

  planet: IPlanet;

  constructor(private activatedRoute: ActivatedRoute, private planetsService: PlanetsService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      flatMap((
        paramMap => this.planetsService.getPlanet(paramMap.get('planetId')
        )))
    ).subscribe( data => { 
      this.planet = data;
      console.log(this.planet);
    });
  }

}