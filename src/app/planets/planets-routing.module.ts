import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanetsPage } from './planets.page';

const routes: Routes = [
  {
    path: '',
    component: PlanetsPage
  },
  {
    path: 'planet-details',
    loadChildren: () => import('./planet-details/planet-details.module').then( m => m.PlanetDetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanetsPageRoutingModule {}
