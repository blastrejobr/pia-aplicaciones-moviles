import { EpisodesService } from './episodes.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.page.html',
  styleUrls: ['./episodes.page.scss'],
})
export class EpisodesPage implements OnInit {

  episodes = [];

  constructor(private episodesService: EpisodesService) { }

  ngOnInit() {
    this.episodesService.getURL().subscribe(
      data => {
        this.episodes = data;
      }
    );
  }

}