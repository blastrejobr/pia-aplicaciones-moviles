import { EpisodesService } from './../episodes.service';
import { ActivatedRoute } from '@angular/router';
import { IEpisode } from './../episodes.model';
import { Component, OnInit } from '@angular/core';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'app-episode-details',
  templateUrl: './episode-details.page.html',
  styleUrls: ['./episode-details.page.scss'],
})
export class EpisodeDetailsPage implements OnInit {

  episode: IEpisode;

  constructor(private activatedRoute: ActivatedRoute, private episodesServices: EpisodesService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      flatMap((
        paramMap => this.episodesServices.getEpisode(paramMap.get('episodeId')
      )))
    ).subscribe( data => {
      this.episode = data;
      console.log(this.episode);
    });
  }

}