import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Episode } from './episodes.model';

@Injectable({
  providedIn: 'root'
})
export class EpisodesService {

  episodes: Episode[];
  apiURL = 'https://rickandmortyapi.com/api/episode/';

  constructor(private http: HttpClient) { }

  getURL(): Observable<any>{
    return this.http.get<any>(`${this.apiURL}`)
    .pipe(map(data => data.results as any));
  }
  
  getEpisodes() {
    this.getURL().subscribe(res => {
      this.episodes = res.results;
      console.log(this.episodes);
    });
    return this.episodes
  }
  
  getEpisode(episodeId: string): Observable<Episode> {
    return this.http.get<any>(`${this.apiURL}/${episodeId}/`)
    .pipe(map(data => data as Episode));
  }

}
