export class Episode{
    constructor(
        public id: string,
        public name: string,
        public air_date: string,
        public episode: string,
        public characters: string[],
        public url: string,
        public created: string
    ){}
}

export interface IEpisode{
    id: string;
    name: string;
    air_date: string;
    episode: string;
    characters: string[];
    url: string;
    created: string;
}