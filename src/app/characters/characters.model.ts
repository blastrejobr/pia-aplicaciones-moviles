export class Character{
    constructor(
        public id: number,
        public name: string,
        public status: string,
        public species: string,
        public type: string,
        public gender: string,
        public origin: characterObject,
        public location: characterObject,
        public image: string,
        public episode: string[],
        public url: string,
        public created: Date
    ){}
}
  
export class characterObject {
    constructor(
      public name: string,
      public url: string
    ){}
}

export interface ICharacter {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: characterObjeto;
  location: characterObjeto;
  image: string;
  episode: string[];
  url: string;
  created: Date;
}

export interface characterObjeto {
  name: string;
  url: string;
}