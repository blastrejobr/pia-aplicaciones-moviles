import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Character } from './characters.model';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  characters: Character[];
  apiURL = 'https://rickandmortyapi.com/api/character/';

  constructor(private http: HttpClient) { }

  getURL(): Observable<any>{
    return this.http.get<any>(`${this.apiURL}`)
    .pipe(map(data => data.results as any));
  }

  getCharacters() {
    this.getURL().subscribe(res => {
      this.characters = res.results;
      console.log(this.characters);
    });
    return this.characters
  }

  getCharacter(characterId: string): Observable<Character> {
      return this.http.get<any>(`${this.apiURL}/${characterId}/`)
      .pipe(map(data => data as Character));
  }

}