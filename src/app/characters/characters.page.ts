import { CharactersService } from './characters.service';
import { Router } from '@angular/router';
import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.page.html',
  styleUrls: ['./characters.page.scss'],
})
export class CharactersPage implements OnInit {

  characters = [];

  constructor(private charactersService: CharactersService) { }

  ngOnInit() {
    this.charactersService.getURL().subscribe(
      data => {
        this.characters = data;
      }
    );
  }

}