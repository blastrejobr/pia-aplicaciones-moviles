import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'characters',
    children: [
      {
        path: "",
        loadChildren: () => import('./characters/characters.module').then( m => m.CharactersPageModule)
      },
      {
        path: ":characterId",
        loadChildren: () => import('./characters/character-details/character-details.module').then( m => m.CharacterDetailsPageModule)
      }
    ]
  },
  {
    path: 'planets',
    children: [
      {
        path: "",
        loadChildren: () => import('./planets/planets.module').then( m => m.PlanetsPageModule)
      },
      {
        path: ":planetId",
        loadChildren: () => import('./planets/planet-details/planet-details.module').then( m => m.PlanetDetailsPageModule)
      }
    ]
    
  },
  {
    path: 'episodes',
    children: [
      {
        path: "",
        loadChildren: () => import('./episodes/episodes.module').then( m => m.EpisodesPageModule)
      },
      {
        path: ":episodeId",
        loadChildren: () => import('./episodes/episode-details/episode-details.module').then( m => m.EpisodeDetailsPageModule)
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
